package edu.upc.damo.toDoList;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity {
    ArrayAdapter<CharSequence> adaptador;
    private EditText text;
    private ListView listView;
    private List<CharSequence> dades = new ArrayList<CharSequence>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.llista);

        inicialitza();
        if (savedInstanceState == null)
            carregaDades();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putCharSequenceArrayList("DADES", (ArrayList<CharSequence>) dades);
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.size() != 0) {
            super.onRestoreInstanceState(savedInstanceState);
            dades.addAll(savedInstanceState.getCharSequenceArrayList("DADES"));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void carregaDades() {

        CharSequence[] dadesEstatiques = getResources().getStringArray(R.array.dadesEstatiques);

        Collections.addAll(dades, dadesEstatiques);
    }


    private void inicialitza() {

        listView = (ListView) findViewById(R.id.llista);
        adaptador = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_list_item_1, dades);
        listView.setAdapter(adaptador);

        text = (EditText) findViewById(R.id.text);
        text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_GO:
                        novesDadesIntroduïdes();
                }
                return false;
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // Esborra l'objecte polsat
                //    dades.remove((int)parent.getAdapter().getItemId(position));
                dades.remove((int) adaptador.getItemId(position));

                // Esborra un objecte que coincideixi amb el polsat
                //    dades.remove(adaptador.getItem(position));
                //      ((ArrayAdapter<String>)parent.getAdapter()).remove((String)parent.getAdapter().getItem(position));
                // adaptador.remove(adaptador.getItem(position));
                //

                adaptador.notifyDataSetChanged();
                return false;  //????????????????
            }
        });

    }

    private void novesDadesIntroduïdes() {
        afegeixContingut(text.getText().toString());
        text.setText("");
    }

    private void afegeixContingut(String s) {
        adaptador.add(s);
    }
}
